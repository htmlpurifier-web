#!/bin/bash
VERSION="$1"
if [ "$VERSION" = "" ]
then
  echo "Please specify version to upgrade to."
  exit
fi

# Download necessary files
DIR="phorum-$VERSION"
FILE="$DIR.tar.gz"
wget "http://www.phorum.org/downloads/$FILE"
tar -xzf "$FILE"
rm "$FILE"
rm -r phorum-original
mv "$DIR" phorum-original

# Install the updates, with patches
sh phorum-patch.sh
sh phorum-skin-patch.sh
