-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

HTML Purifier 4.12.0 is a maintenance release which makes
compatibility fixes for PHP 7.4.

Release notes for 4.12.0:
    https://github.com/ezyang/htmlpurifier/blob/v4.12.0/NEWS

Download links for 4.12.0:
    http://htmlpurifier.org/releases/htmlpurifier-4.12.0.tar.gz
    http://htmlpurifier.org/releases/htmlpurifier-4.12.0.zip

SHA-1 sums:
    12afe93b88b6f0577f76ff1a72983d9c8efe6ca3  htmlpurifier-4.12.0.tar.gz
    8415a68696e9b0078838717183f3b2b01ba6c6ff  htmlpurifier-4.12.0.zip

Other downloads (standalone and lite):
    http://htmlpurifier.org/download.html
-----BEGIN PGP SIGNATURE-----

iF0EARECAB0WIQQ/qOmpc4W2kab8s8upM759hpxI2gUCXbZlqgAKCRCpM759hpxI
2thzAJ4/Qfyp7s443pAkJvgsRe3Sx4QfowCghaYIJvsQ6uFqrAR0dgsSAbKXXwc=
=Z2ky
-----END PGP SIGNATURE-----
