#!/bin/bash

# To minimize code duplication, there are numerous symlinks setup in the root.
# In order to ensure continued functionality, these must be recreated if we
# perform a fresh install of htmlpurifier-web

ln -s "$HOME/htmlpurifier.org/dev/plugins/phorum"                  "phorum/mods/htmlpurifier"
ln -s "$HOME/htmlpurifier.org/misc/phorum/htmlpurifier-custom"     "phorum/mods/htmlpurifier-custom"
ln -s "$HOME/htmlpurifier.org/misc/phorum/htmlpurifier-akismet"    "phorum/mods/htmlpurifier-akismet"
