function open_delicious() {
    window.open(
        'http://del.icio.us/post?v=4&noui&jump=close&url='+
        encodeURIComponent(location.href)+
        '&title='+encodeURIComponent(document.title),
        'delicious','toolbar=no,width=700,height=400');
    return false;
}

function setup_delicious() {
    var link = document.getElementById('delicious');
    link.onclick = open_delicious;
}

if (window.addEventListener) {
    window.addEventListener("load", setup_delicious, false);
} else if (window.attachEvent) {
    window.attachEvent("onload", setup_delicious);
}