#!/bin/bash
cd phorum/templates
if [ -d temp ]
then
    rm -r temp
fi
cp -R lightweight temp
cd temp
patch -u -p 1 < ../../../phorum-skin.patch
cd ../
if [ -d htmlpurifier.bak ]
then
    rm -r htmlpurifier.bak
fi
if [ -d htmlpurifier ]
then
    mv htmlpurifier htmlpurifier.bak
fi
mv temp htmlpurifier
cd ../../
