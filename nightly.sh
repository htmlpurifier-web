#!/bin/bash

cd /home/ezyang
./mirror.sh
cd htmlpurifier.org/releases
rm htmlpurifier-trunk*
./build.sh tar trunk /home/ezyang/git.htmlpurifier.org/htmlpurifier.git
./build.sh zip trunk /home/ezyang/git.htmlpurifier.org/htmlpurifier.git
