<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html
  xmlns="http://www.w3.org/1999/xhtml"
  xmlns:xi="http://www.w3.org/2001/XInclude"
  xml:lang="en">
<head>
  <title>Contribute - HTML Purifier</title>
  <xi:include href="common-meta.xml" xpointer="xpointer(/*/node())" />
  <meta name="description" content="How to help HTML Purifier grow through code and attention." />
  <meta name="keywords" content="HTMLPurifier, HTML Purifier, HTML, filter, filtering, standards, compliant, contribute, contribution, open source, community, help, code, needed" />
</head>
<body>

<xi:include href="common-header.xml" xpointer="xpointer(/*/node())" />

<div id="main">
<h1 id="title">Contribute</h1>

<div id="content">

<p>
  The very first question to ask yourself before reading this page is this:
</p>

<blockquote><p><em>Why contribute?</em></p></blockquote>

<p>
  As open-source software, you are not legally obligated to give anything
  back to the community.  In such a sense, HTML Purifier is our gift to
  you, and you very well can run away and never be heard from again.
</p>

<p>
  We hope, however, that this lack of a legal obligation doesn't prevent
  you from contributing back to our project.  We poured many hours into
  this project, and doubtless, this project has saved
  many hours on your behalf.  If HTML Purifier saved you 200 hours of work
  (the actual figure might be more, might be less), even if you contribute
  ten hours back to the project, you still come out ahead 190 hours.
</p>

<p>
  Additionally, your use of this library also requires substantial investment
  on your part as well.  You were required to learn the APIs, read the
  documentation, tweak things so that they worked with your application,
  et cetera.  Contributing back means making good use of this investment:
  it means not only will your expertise and knowledge be fed back into
  HTML Purifier, but you might learn a thing or to from the internals that
  you didn't know before.
</p>

<p>
  If I've convinced you, read on! It's quite easy to get started...
</p>

<div id="toc" />

<h2>What can you do?</h2>

<p>
  Contributions can come in many forms.  Documentation, code, even
  evangelism, can all help a project.  One of the things we've noticed,
  however, is that many contributions come from people helping
  themselves.  They have an itch, a special requirement, and they help
  the project out in that area.
</p>

<p>
  What might that itch be? Over the years, we've accumulated many feature
  requests in our <a href="dev/TODO">TODO</a> file.  There are also
  tasty tidbits in the <a href="docs">proposal section of our
  documentation.</a> You might have an
  idea for a new AutoFormatter, or maybe would like to implement an HTMLModule
  for a set of elements that HTML Purifier doesn't support yet.  Maybe you 
  want a demo page built-in with the library so that you can easily test
  things out without using HTML Purifier's demo page.  Code something that
  interests you.
</p>

<h2>Coding standards</h2>

<p>
  As a general rule of thumb, make sure your code looks like the code around
  it.  Probably the biggest thing is to remember four spaces, no tabs (if you
  perpetually forget, get your text-editor to make whitespace visible). There
  are a number of other formatting subtleties, but suffice to say
  <em>consistency</em> is the order of the day in this project. You're not
  going to read <acronym title="Yet Another Coding Standard">YACS</acronym> anyway.
</p>

<p>
  The code you write must be PHP 5.0.5 compatible, so avoid later features
  like magic methods.  The code you write also must have unit tests, which
  reside in the <em>tests/</em> directory.  The workflow for your feature
  should be along the lines of:
</p>

<ol>
  <li>Write unit tests</li>
  <li>Hack hack hack</li>
  <li>Run <em>php tests/index.php</em></li>
  <li>If failures, go back to 1 or 2</li>
  <li>Commit and submit patch</li>
</ol>

<p>
  HTML Purifier prides itself in having an evergreen test suite, so if your
  change breaks other tests, it probably won't be accepted.
</p>

<h2>Getting setup</h2>

<p>
  You already know how to <em>use</em> HTML Purifier. But do you know how
  to develop it?
</p>

<h3>Git</h3>

<p>
  HTML Purifier's repository is hosted via Git. If you've used Git before,
  you can skip this section: you already know what the workflow is for
  working on Git, so just clone from <em>git://repo.or.cz/htmlpurifier.git</em> and
  get going. Otherwise, read-on.
</p>

<p>
  In order to hack on HTML Purifier's source tree, you will first need to
  make sure Git is installed on your system. Type the following command
  in your prompt:
</p>

<pre class="command"><a href="http://www.kernel.org/pub/software/scm/git/docs/">git</a> --version</pre>

<p>
  And you should get something along the lines of <q>git version 1.5.6</q>.
  Otherwise:
</p>

<dl>
  <dt>You use Linux:</dt>
  <dd>
    Grab Git from your friendly neighborhood package manager. Or compile
    from source with package provided at <a href="http://git.or.cz/">git.or.cz</a>.
    Either should be relatively simple.
  </dd>
  <dt>You use Windows:</dt>
  <dd>
    Download and install <a href="http://code.google.com/p/msysgit/">msysgit</a>.
    Then, for all of the following commands
    we discuss, enter them in the console provided by Git Bash. If you have
    Cygwin, you can also use setup.exe to install Git.
  </dd>
  <dt>You use a Mac:</dt>
  <dd>
    There are binaries available from <a href="http://metastatic.org/text/Concern/2007/09/15/new-git-package-for-os-x/">various</a>
    <a href="http://code.google.com/p/git-osx-installer/">sources</a>; I haven't
    tried them so your mileage may vary. Since Mac is a BSD-like system, you
    can also <a href="http://www.dekorte.com/blog/blog.cgi?do=item&amp;id=2539">compile
    from source.</a>
  </dd>
</dl>

<p>
  Run the earlier command again to make sure the installation went
  smoothly. Now run this command:
</p>

<pre class="command"><kbd><a href="http://www.kernel.org/pub/software/scm/git/docs/git-clone.html">git clone</a> git://repo.or.cz/htmlpurifier.git</kbd></pre>

<p>
  This will copy the HTML Purifier codebase into the htmlpurifier folder.
</p>

<p>
  You will want to configure the Git installation with your name and
  email address. You can do this with these two commands.
</p>

<pre class="command"><kbd><a href="http://www.kernel.org/pub/software/scm/git/docs/git-config.html">git config</a> --global user.name "Bob Doe"
git config --global user.email bob@example.com</kbd></pre>

<p>
  Let us fast forward for a moment and imagine that we already made our changes
  and would now like to send the changes to HTML Purifier for review. You
  will to execute these commands:
</p>

<pre class="command"><kbd><a href="http://www.kernel.org/pub/software/scm/git/docs/git-status.html">git status</a></kbd></pre>

<p>
  This command will give you a quick rundown about all the files Git knows
  about. If you have any <q>Untracked files</q>, you will need to add
  them with:
</p>

<pre class="command"><kbd><a href="http://www.kernel.org/pub/software/scm/git/docs/git-add.html">git add</a> <em>$filename</em></kbd></pre>

<blockquote class="aside"><p>
  (You can also add <q>Changed but not updated</q> files, but because we will
  be using the <kbd>-a</kbd> option this is strictly unnecessary.)
</p></blockquote>

<p>
  Now, you will want to commit your changes.  Users of centralized version
  control systems, beware: this does not push it to a remote repository,
  or anything like that. It simply records the change in your local repository.
  Doing so is as simple as:
</p>

<pre class="command"><kbd><a href="http://www.kernel.org/pub/software/scm/git/docs/git-commit.html">git commit</a> -as</kbd></pre>

<blockquote class="aside"><p>
  The <q>a</q> flag tells Git to commit all modified files, even if you didn't
  git add them. The <q>s</q> flag tells Git to sign off your commit message
  with your name and email.
</p></blockquote>

<p>
  You will then have a screen brought up to enter a commit message. If this
  screen is vim (you can tell if your command line window transmuted into
  something you've never seen before), type <kbd>i</kbd> (<samp>--INSERT--</samp>
  mode), write your commit message, type <kbd>ESC</kbd>, and
  then type <kbd>:wq ENTER</kbd> (write and quit).
</p>

<p>
  A quick note about commit messages: there is a very specific format for them.
  They should look something like this:
</p>

<pre><samp>Concise one-line statement describing change

Full explanation for the change. If you fixed a bug, make
sure you describe what was wrong, how you fixed it, and
what the behavior is now. If it was a feature, describe
why the feature is useful, how you use it, and any tricky
implementation details.

In short, the body of the commit message (which can span multiple
paragraphs) should, along with the code diff, be self
explanatory and not require any email introduction. At the
same time, your commit message will be immortalized and
should be in third-person and formal.

Signed-off-by: Edward Z. Yang &lt;edwardzyang@thewritingpot.com&gt;</samp></pre>

<p>
  Finally, after the commit has been recorded, you will want to make a
  patch to distribute to other people to review and test. Doing so is
  as simple as:
</p>

<pre class="command"><a href="http://www.kernel.org/pub/software/scm/git/docs/git-format-patch.html">git format-patch</a> -1</pre>

<blockquote class="aside"><p>
  You can substitute -1 for -#, where # is the number of commits you would
  like to write patches for. You can also specify a commit hash ID.
</p></blockquote>

<p>
  A file named roughly <em>0001-Short-description.patch</em> will be
  created, with the complete contents of your change.
</p>

<p>In summary:</p>

<pre class="command"><kbd>git clone git://repo.or.cz/htmlpurifier.git
git config --global user.name "Bob Doe"
git config --global user.email bob@example.com
cd htmlpurifier</kbd>
# hack hack hack
<kbd>git status
git add newfile1.txt subdir/newfile2.txt
git commit -as
git format-patch -1
# send patch off</kbd></pre>

<p>
  Two quick notes before we go on to some HTML Purifier specific instructions:
</p>

<ol>
  <li>
    <p>
      If you are posting the patch on the forum, be sure to copy-paste it
      in-between <code>&lt;pre&gt;&lt;![CDATA[</code> and <code>]]&gt;&lt;/pre&gt;</code>
      If you are emailing the patch, we prefer that you send it inline in a text
      email (be sure to configure your mail client not to wrap lines, check out
      <a href="http://repo.or.cz/w/git.git?a=blob;f=Documentation/SubmittingPatches;hb=HEAD">SubmittingPatches guidelines from the Git project</a> for more details.)
    </p></li>
  <li>
    <p>
      In all probability, there have been changes to the HTML Purifier codebase
      since you made your patch.  As part of your duties as a patch-maker, you
      should ensure that your patch remains off of the HEAD of our master branch.
      You can do so with the command:
    </p>
    <pre class="command"><a href="http://www.kernel.org/pub/software/scm/git/docs/git-pull.html">git pull</a> --rebase</pre>
    <p>
      You may also find it useful to perform your development in a topic branch.
      You can do this using:
    </p>
    <pre class="command"><a href="http://www.kernel.org/pub/software/scm/git/docs/git-checkout.html">git checkout</a> -b <em>branchname</em></pre>
    <p>
      The benefits of a setup like this is you can now do a regular
      <kbd>git pull</kbd> on the master branch, and then use 
      <kbd><a href="http://www.kernel.org/pub/software/scm/git/docs/git-rebase.html">git rebase</a> master</kbd> on your own branch to keep it up to
      date. This can be useful if your patch produces a conflict.
      (One quick note; you switch between branches using <kbd>git
      checkout <em>branchname</em></kbd>. The -b flag creates a new branch.)
    </p>
    <blockquote class="aside"><p>
      The default behavior of <kbd>git pull</kbd> in such a case is to merge
      your branch. If you were a release maintainer, this is what you would
      want to do, since your history was public and rewriting history
      could be disruptive. With private, local changes, however, performing
      the merge makes the history needlessly complicated.
    </p></blockquote>
  </li>
</ol>

<h3>SimpleTest</h3>

<p>
  As mentioned before, one of the keys to successfully developing a new
  feature on HTML Purifier is a comprehensive set of unit tests.  However,
  unit tests serve you no good if you can't run them.
</p>

<p>
  The first step in getting unit tests running on HTML Purifier is downloading
  <a href="http://simpletest.org">SimpleTest</a>, our test suite.  However,
  the public 1.0.1 release won't work with HTML Puriifer, as it is still
  <abbr>PHP</abbr>4 compatible and will give off spurious errors. You need to
  use the trunk version of SimpleTest. This version can be checked out
  using <a href="http://subversion.tigris.org/">Subversion</a> with this command:
</p>

<pre class="command"><kbd>svn co https://simpletest.svn.sourceforge.net/svnroot/simpletest/simpletest/trunk simpletest</kbd></pre>

<p>
  The next step is to tell HTML Purifier about the SimpleTest installation.
  You can do this by copying the <em>test-settings.sample.php</em> file 
  to <em>test-settings.php</em> and configuring it according to the
  instructions inside.  The only variable you must edit is 
  <var>$simpletest_location</var>.
</p>

<blockquote><p>
  At the moment, it is somewhat difficult to get the optional parameters setup
  properly.  If you feel adventurous, try the instructions; they should work,
  but might be a little complicated or sparser than usual.
</p></blockquote>

<p>
  Now, check if everything is running by typing <kbd>php tests/index.php --flush</kbd>
  from the root of your HTML Purifier working copy. You should get a full
  complement of passing tests. Congratulations!
</p>

<h2>Workflow</h2>

<p>
  After identifying what changes you would like to make to HTML Purifier,
  you will need to code appropriate unit tests for it. (If you are of the
  code first, test later mentality, that is fine too; just make sure the tests
  are 1. written and 2. comprehensive.) If you modify the file
  <em>library/HTMLPurifier/ConfigSchema.php</em>, chances are the corresponding
  tests are in <em>tests/HTMLPurifier/ConfigSchemaTest.php</em> (i.e. substitute
  library with tests and append a Test to the filename.)
</p>

<p>
  We prefer, first-and-foremost, <em>unit</em> tests, that is, the test should
  not have any dependencies on any other objects, and if it does, those
  dependencies should be filled in using SimpleTest's excellent 
  <a href="http://www.lastcraft.com/mock_objects_documentation.php">mock object support</a>.
  We also believe strongly in integration tests,
  which take in the form of htmlt files, and test HTML Purifier as a whole
  with your modifications. An htmlt file looks like this:
</p>

<pre><samp><![CDATA[--INI--
%HTML.Allowed = "b,i,u,p"
--HTML--
<b>Foo<a id="asdf">bar</a></b>
--EXPECT--
<b>Foobar</b>
]]></samp></pre>

<p>
  The <samp>--INI--</samp> section indicates the configuration directives
  that should be used with this test (if you added a new feature, you will
  most probably be using this section to activate it). The <samp>--HTML--</samp>
  section indicates the input, and the <samp>--EXPECT--</samp> indicates
  the expected output. Be sure to include a trailing newline. You can place
  these files in the <em>tests/HTMLPurifier/HTMLT</em> directory; give them
  a descriptive filename.
</p>

<p>
  It is my hope that you find the HTML Purifier core code a joy (or at least,
  not painful) to work with; every class and method has a docblock that doesn't
  reiterate what you can find inside its body, but also how the component
  fits into HTML Purifier as a whole. If you find any section of code that
  is missing or has poor documentation, please notify us and we will
  correct it immediately. (Remember, <kbd>git pull --rebase</kbd> to
  update your branch!)
</p>

<p>
  There are, however, some architectural features that are not immediately
  evident from mere source-code browsing. In this case, you are encouraged
  to check out the documentation in the <em>docs/</em> folder (web
  accessible at <a href="docs/">the same location.</a>) 
  <a href="docs/dev-flush.html"><q>Flushing the Purifier</q></a>
  and <a href="docs/dev-config-schema.html"><q>Config Schema</q></a> in the Development center are of particular
  notability: in all likelihood you will need this knowledge in order to
  get HTML Purifier working the way you want it to.
</p>

<h2>Debugging</h2>

<p>
  Your debugging skills are as good as
  mine, but there are few things that are helpful to keep in mind:
</p>

<ul>
  <li>
    You can modify the granularity of tests to run down to a single
    test-case method.  The first method is to specify the <em>f</em>
    parameter with a value like <samp>HTMLPurifier/ConfigSchemaTest.php</samp>
    which will cause HTML Purifier to run only that test. (In web URL
    speak, this means <em>tests/index.php?f=HTMLPurifier/ConfigSchemaTest.php</em>,
    in command line speak, this means <kbd>php tests/index.php -f HTMLPurifier/ConfigSchemaTest.php</kbd>.
    To run only a single test <em>method</em>, prefix that method with
    <code>__only</code>. Be sure to revert this change when you're done
    hammering away, and don't forget to test <em>everything</em> before committing.
  </li>
  <li>
    HTML Purifier does not have a debugging/verbose mode, so any internal
    data-checks need to be <code>var_dump</code>'ed by the user.
    <a href="http://www.xdebug.org/">XDebug</a> makes var_dump'ing a pleasure
    by colorizing and escaping output. (The stack traces are also quite
    handy!) There is also a function called <code>printTokens($tokens, $index)</code> specifically
    for outputting arrays of tokens. The <var>$index</var> variable
    indicates a token to make bold, and can be omitted.
  </li>
  <li>
    There's a Debugger class. Don't use it. It kinda sucks.
  </li>
  <li>
    If it seems like a change you made had no effect on your tests, try
    flushing with <em>flush</em>.
  </li>
  <li>
    SimpleTest's error message when an <code>assertIdentical</code> message fails with
    strings is incomprehensible, so keep your test strings small or be ready to
    <code>var_dump</code> if necessary.
  </li>
  <li>
    Beware whitespace. Tests should work whether or not they're Unix (LF), Windows (CRLF)
    or Mac (CR) encoded. This usually means <em>not</em> using <code>PHP_EOL</code>
    but rather a literal newline in the source code.
  </li>
</ul>

</div>
</div>
</body>
</html>
