#!/bin/bash
if [ "$1" == "" ]
then
    echo "Usage: ./sum-all.sh version"
    exit
fi
sha1sum "htmlpurifier-$1"* > ../current-hashes.txt
