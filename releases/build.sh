#!/bin/bash

# Build script for HTML Purifier distributions. Syntax:
#
#   ./build.sh (tar|zip) 1.2.3 git://repo.or.cz/htmlpurifier.git
#
# Tar will actually produce tar.gz, and the repository third parameter can
# be replaced with any valid repository path that contains the necessary tags.

source $HOME/.bashrc

FORMAT="$1"
VERSION="$2"
REPO="$3"
NAME="htmlpurifier-$VERSION"

copy_meta_files() {
    cp "$NAME/LICENSE" "$SNAME"
    cp "$NAME/NEWS" "$SNAME"
    cp "$NAME/INSTALL" "$SNAME"
    cp "$NAME/CREDITS" "$SNAME"
}

archive() {
    if [ "$FORMAT" = "zip" ]
    then
        zip -q -r "$1.zip" "$1"
    else
        tar -cf - "$1" |  gzip -c > "$1.tar.gz"
    fi
}

if [ "$FORMAT" = "" ]
then
  echo "Format of tar or zip must be specified in first param"
  exit
fi

if [ "$VERSION" = "" ]
then
  echo "Version must be specified in second param"
  exit
fi

if [ "$REPO" = "" ]
then
  REPO="git://repo.or.cz/htmlpurifier.git"
fi
  
if [ "$VERSION" = "trunk" ]
then
    REV="master"
else
    REV="v$VERSION"
fi

git clone -n "$REPO" "$NAME"
cd "$NAME"

CRLF=`git config core.autocrlf`
if [ "$FORMAT" = "zip" ]
then
    git config core.autocrlf true
else
    git config core.autocrlf false
fi
git checkout "$REV"
rm -rf .git
cd ..

archive "$NAME"

SNAME="$NAME-lite"
mkdir "$SNAME"
cp -R "$NAME/library" "$SNAME"
copy_meta_files
archive "$SNAME"
rm -R "$SNAME"

SNAME="$NAME-standalone"
mkdir "$SNAME"
export PHP_IS_CLI=1
php "$NAME/maintenance/generate-standalone.php"
mv "$NAME/library/HTMLPurifier.standalone.php" "$SNAME"
mv "$NAME/library/standalone" "$SNAME"
rm -Rf "$NAME/tests/blanks/*"
archive "$SNAME"
rm -R "$SNAME"

rm -R "$NAME"

git config core.autocrlf "$CRLF"
