#!/bin/bash
if [ "$1" == "" ]
then
    echo "Usage: ./sign-all.sh version"
    exit
fi
VERSION=$1
#KEY=0x1E1C674B
KEY=0x869C48DA
gpg -u $KEY -b htmlpurifier-$VERSION.tar.gz
gpg -u $KEY -b htmlpurifier-$VERSION-lite.tar.gz
gpg -u $KEY -b htmlpurifier-$VERSION-standalone.tar.gz
gpg -u $KEY -b htmlpurifier-$VERSION.zip
gpg -u $KEY -b htmlpurifier-$VERSION-lite.zip
gpg -u $KEY -b htmlpurifier-$VERSION-standalone.zip
