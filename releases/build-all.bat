@echo off

if [%1] neq [] (set REPO=%1) else (set /p REPO=Repository? )
if [%2] neq [] (set VERSION=%2) else (set /p VERSION=Version? )

git clone -n "%REPO%" htmlpurifier-tmp
bash build.sh tar "%VERSION%" htmlpurifier-tmp
bash build.sh zip "%VERSION%" htmlpurifier-tmp
rmdir /S /Q htmlpurifier-tmp
