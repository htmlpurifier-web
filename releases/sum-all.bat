@echo off

if [%1] neq [] (set VERSION=%1) else (set /p VERSION=Version? )
sha1sum htmlpurifier-%VERSION%* > ../current-hashes.txt
set /p D=Press enter to close
