@echo off

if [%1] neq [] (set VERSION=%1) else (set /p VERSION=Version? )
gpg -u 0x1E1C674B -b htmlpurifier-%VERSION%.tar.gz
gpg -u 0x1E1C674B -b htmlpurifier-%VERSION%-lite.tar.gz
gpg -u 0x1E1C674B -b htmlpurifier-%VERSION%-standalone.tar.gz
gpg -u 0x1E1C674B -b htmlpurifier-%VERSION%.zip
gpg -u 0x1E1C674B -b htmlpurifier-%VERSION%-lite.zip
gpg -u 0x1E1C674B -b htmlpurifier-%VERSION%-standalone.zip
