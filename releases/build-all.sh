#!/bin/bash
if [ $# -ne 2 ]
then
    echo "Usage: ./build-all.sh repo version"
    exit
fi
REPO=$1
VERSION=$2
git clone -n "$REPO" htmlpurifier-tmp
./build.sh tar "$VERSION" htmlpurifier-tmp
./build.sh zip "$VERSION" htmlpurifier-tmp
rm -rf htmlpurifier-tmp
