#!/bin/sh
if [ $# -ne 1 ]
then
    echo "Usage: ./upload-all.sh version"
    exit
fi
VERSION=$1
scp htmlpurifier-$1* athena.dialup.mit.edu:~/web_scripts/htmlpurifier/releases
#scp htmlpurifier-$1* htmlpurifier.org:/home/ezyang/htmlpurifier.org/releases
