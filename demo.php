<?php

// major TODO: hook into some sort of templating system that utilizes
// XHTML Compiler to process the template (cacheable, of course), before
// passing it along to this script

// using _REQUEST because we accept GET and POST requests

// can break XML handling (ideally we should place these errors
// somewhere the user can see, so this is a stopgap)
error_reporting(0);

function getFormMethod() {
    return (isset($_REQUEST['post'])) ? 'post' : 'get';
}
function escapeHTML($html) {
    return htmlspecialchars(
        HTMLPurifier_Encoder::cleanUTF8($html), ENT_COMPAT, 'UTF-8');
}
function isLocal() {
    static $name = false;
    if (!$name) {
        if (file_exists($dir = 'xhtml-compiler/conf/name.txt')) {
            $name = trim(file_get_contents($dir));
        } elseif (file_exists('xhtml-compiler/local.txt')) {
            $name = 'EZYANG';
        } elseif (file_exists('local.txt')) {
            $name = 'EZYANG';
        }
    }
    return $name == 'EZYANG' || $name == 'EZYANG2';
}

if (empty($_REQUEST['experimental'])) {
    require_once 'live/library/HTMLPurifier.auto.php';
} elseif (!isLocal()) {
    require_once 'dev/library/HTMLPurifier.auto.php';
} else {
    require_once '../htmlpurifier/library/HTMLPurifier.auto.php';
}
require_once 'HTMLPurifier/Printer/ConfigForm.php';

// Permissible configuration options have to be chosen carefully; HTML
// Purifier wasn't designed for unfriendly user configuration, so
// certain configuration directives can be used to cause XSS attacks
// because they're not validated at all.

$allowed_lite = array(
    'Core.CollectErrors',
    'URI.DisableExternalResources',
    'URI.Munge',
    'HTML.TidyLevel',
    'HTML.Doctype',
    'HTML.Allowed',
    'HTML.SafeObject',
    'HTML.FlashCompat',
    'CSS.AllowedProperties',
    'AutoFormat',
    '-AutoFormat.Custom',
    '-AutoFormat.PurifierLinkify',
    '-AutoFormat.PurifierLinkify.DocURL',
    '-AutoFormat.RemoveEmpty.RemoveNbsp',
    '-AutoFormat.RemoveEmpty.RemoveNbsp.Exceptions'
);

$config = HTMLPurifier_Config::loadArrayFromForm($_REQUEST, 'filter', $allowed_lite);
$purifier = new HTMLPurifier($config);

if (!empty($_REQUEST['strict'])) {
    // backwards-compatibility
    // (muting deprecated error)
    @$config->set('HTML', 'Strict', true);
}
if (!empty($_REQUEST['experimental'])) {
    //require_once 'HTMLPurifier/Lexer/PH5P.php';
    //$config->set('Core', 'LexerImpl', 'PH5P');
    //$config->set('HTML.SafeObject', true);
    //$config->set('HTML.FlashCompat', true);
}

if (file_exists('demo.custom.php') && isLocal()) include 'demo.custom.php';

$definition = $config->getHTMLDefinition();
$doctype = $definition->doctype;

if ($doctype->xml && stripos($_SERVER["HTTP_ACCEPT"], 'application/xhtml+xml') !== false && !isset($_REQUEST['debug'])) {
    $type = 'application/xhtml+xml';
} else {
    $type = 'text/html';
}
header("Content-type:$type;charset=UTF-8");

// prevent PHP versions with shorttags from barfing
if ($doctype->xml) {
    echo '<?xml version="1.0" encoding="UTF-8" ?>' . PHP_EOL;
}

if (!empty($doctype->dtdPublic) && !empty($doctype->dtdSystem)) {
    echo '<!DOCTYPE html PUBLIC "'.$doctype->dtdPublic.'" "'.$doctype->dtdSystem.'">' . PHP_EOL;
}

if ($doctype->xml) {
    echo '<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">' . PHP_EOL;
    $END = ' /';
} else {
    echo '<html lang="en">' . PHP_EOL;
    $END = '';
}

?><head>
    <title>HTML Purifier Live Demo</title>
    <!-- make sure all empty elements that are not generated are
     appropriately ended -->
    <meta name="author" content="Edward Z. Yang"<?php echo $END; ?>>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"<?php echo $END; ?>>
    <link rel="icon" href="favicon.ico" type="image/x-icon"<?php echo $END; ?>>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon"<?php echo $END; ?>> 
    <link rel="stylesheet" href="common.css" type="text/css"<?php echo $END; ?>>
    <link rel="stylesheet" href="demo.css" type="text/css"<?php echo $END; ?>>
    <link rel="stylesheet" href="live/library/HTMLPurifier/Printer/ConfigForm.css" type="text/css"<?php echo $END; ?>>
    <script defer="defer" type="text/javascript" src="live/library/HTMLPurifier/Printer/ConfigForm.js"></script>
</head>
<body>
<div id="logo"></div>
<div id="header"><a href=".">HTML Purifier</a></div>
<?php
if (file_exists('navigation.frag')) {
    readfile('navigation.frag');
} else { ?>
    <div><strong>Please navigate to <a href="navigation.html">navigation.html</a> to regenerate menu</strong></div>
<?php }
?>
<div id="main">
<h1 id="title">Live Demo</h1>
<div id="content">
<?php

if (!empty($_REQUEST['html'])) { // start result
    
    if (strlen($_REQUEST['html']) > 50000) {
        ?>
        <p>Request exceeds maximum allowed text size of 50kb.</p>
        <?php
    } else { // start main processing
    
    $html = get_magic_quotes_gpc() ? stripslashes($_REQUEST['html']) : $_REQUEST['html'];
    $pure_html = $purifier->purify($html);
    
?>
<p>Here is your purified HTML:</p>
<div id="output">
<?php if(getFormMethod() == 'get') {
// calculate image
$img = false;
if ($doctype->name == 'HTML 4.01 Transitional' || $doctype->name == 'HTML 4.01 Strict') {
    $img = 'http://www.w3.org/Icons/valid-html401';
} elseif ($doctype->name == 'XHTML 1.0 Transitional' || $doctype->name == 'XHTML 1.0 Strict') {
    $img = 'http://www.w3.org/Icons/valid-xhtml10';
} elseif ($doctype->name == 'XHTML 1.1') {
    $img = 'http://www.w3.org/Icons/valid-xhtml11';
}
?>
  <div id="w3c-validator">
    <a href="http://validator.w3.org/check?uri=referer" title="Valid <?php echo escapeHTML($doctype->name); ?>">
    <?php if ($img) { ?>
      <img
        src="<?php echo escapeHTML($img); ?>" height="31" width="88"
        alt="Valid <?php echo escapeHTML($doctype->name); ?>"<?php echo $END; ?>>
    <?php } else { ?>
      Valid <?php echo escapeHTML($doctype->name); ?>
    <?php } ?>
    </a>
  </div>
<?php } ?>
<?php

echo $pure_html;

?>
<div class="clear"></div>
</div>
<?php if (@$config->get('Core', 'CollectErrors')) {
    $e = $purifier->context->get('ErrorCollector');
    $class = $e->getRaw() ? 'fail' : 'pass';
?>
    <p>Here are errors that occurred during purification:</p>
    <div id="errors" class="<?php echo $class ?>">
    <?php 
    echo $e->getHTMLFormatted($config);
    ?>
</div>
<?php } ?>
<p>Here is the source code of the purified HTML:</p>
<pre id="source"><?php echo escapeHTML($pure_html); ?></pre>
<?php
if (getFormMethod() == 'post') { // start POST validation notice
?>
    <p>If you would like to validate the code with
    <a href="http://validator.w3.org/#validate-by-input">W3C's
    validator</a>, copy and paste the <em>entire</em> demo page's source.</p>
<?php
} // end POST validation notice

?>
<p>Share this purification using the <a href="javascript:var%20e=document.createElement('script');e.setAttribute('language','javascript');e.setAttribute('src','http://bit.ly/bookmarklet/load.js');document.body.appendChild(e);void(0);">bit.ly URL shortener</a>.</p>
<?php

} // end main processing

// end result
} else {

?>
<p>Welcome to the live demo.  Enter some HTML and see how HTML Purifier
will filter it.</p>
<?php

}

?>
<form id="filter" action="demo.php<?php
echo '?' . getFormMethod();
if (isset($_REQUEST['profile']) || isset($_REQUEST['XDEBUG_PROFILE'])) {
    echo '&amp;XDEBUG_PROFILE=1';
} ?>" method="<?php echo getFormMethod();  ?>">
    <fieldset>
        <legend>HTML Purifier Input (<?php echo getFormMethod(); ?>)</legend>
        
        <?php
$form_printer = new HTMLPurifier_Printer_ConfigForm('filter', 'http://htmlpurifier.org/live/configdoc/plain.html#%s', 14);
echo $form_printer->render($config, $allowed_lite, false);
        ?>
        
        <div id="textarea">
        <textarea name="html" cols="60" rows="15"><?php
if (isset($html)) echo escapeHTML($html);
        ?></textarea>
        </div>
        
        <p class="lead">
            By default, HTML Purifier may remove some of your spacing or
            indentation. Turn on CollectErrors or experimental features in
            order to fully preserve whitespace.
        </p>
        <?php if (getFormMethod() == 'get') { ?>
            <p><strong>Warning:</strong> GET request method can only hold
                8129 characters (probably less depending on your browser).
                If you need to test anything
                larger than that, try the <a href="?post">POST form</a>.</p>
        <?php } ?>
        <div id="controls">
            <input type="submit" value="Submit" name="submit" class="button"<?php echo $END; ?>>
            Use <abbr class="elaborates" title="This runs code from the master Git repository branch.">experimental features</abbr>: <input type="checkbox" value="1" <?php if(!empty($_REQUEST['experimental'])) {echo 'checked="checked" ';} ?>name="experimental"<?php echo $END; ?>>
        </div>
    </fieldset>
</form>
<p class="lead">Try the form in <a href="?get">GET</a> and <a href="?post">POST</a> request
flavors (GET is easy to validate with W3C, but POST allows larger inputs).
Don't know what to test? Try out these sample filterings:</p>
<ul>
    <li><a href="demo.php?html=%3Cimg+src%3D%22javascript%3Aevil%28%29%3B%22+onload%3D%22evil%28%29%3B%22+%2F%3E">Malicious code removed</a></li>
    <li><a href="demo.php?html=%3Cb%3EBold&amp;submit=Submit">Missing end tags fixed</a></li>
    <li><a href="demo.php?html=%3Cb%3EInline+%3Cdel%3Econtext+%3Cdiv%3ENo+block+allowed%3C%2Fdiv%3E%3C%2Fdel%3E%3C%2Fb%3E&amp;submit=Submit">Illegal nesting fixed</a></li>
    <li><a href="demo.php?html=%3Ccenter%3ECentered%3C%2Fcenter%3E&amp;filter%5BHTML.Doctype%5D=XHTML+1.0+Strict&amp;submit=Submit">Deprecated tags converted</a></li>
    <li><a href="demo.php?html=%3Cspan+style%3D%22color%3A%23COW%3Bfloat%3Aaround%3Btext-decoration%3Ablink%3B%22%3EText%3C%2Fspan%3E&amp;submit=Submit"><abbr>CSS</abbr> validated</a></li>
    <li><a href="demo.php?html=%3Ctable%3E%0D%0A++%3Ccaption%3E%0D%0A++++Cool+table%0D%0A++%3C%2Fcaption%3E%0D%0A++%3Ctfoot%3E%0D%0A++++%3Ctr%3E%0D%0A++++++%3Cth%3EI+can+do+so+much%21%3C%2Fth%3E%0D%0A++++%3C%2Ftr%3E%0D%0A++%3C%2Ftfoot%3E%0D%0A++%3Ctr%3E%0D%0A++++%3Ctd+style%3D%22font-size%3A16pt%3B%0D%0A++++++color%3A%23F00%3Bfont-family%3Asans-serif%3B%0D%0A++++++text-align%3Acenter%3B%22%3EWow%3C%2Ftd%3E%0D%0A++%3C%2Ftr%3E%0D%0A%3C%2Ftable%3E&amp;experimental=1&amp;submit=Submit">Rich formatting preserved</a></li>
</ul>
</div>
</div>
</body>
</html>
