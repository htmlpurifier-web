<?php
require_once 'settings.pear.php';
set_include_path(get_include_path() . PATH_SEPARATOR . '/home/ezyang/usr/lib/php/');
require_once 'PEAR/Config.php';
PEAR_Config::singleton("/home/ezyang/.pearrc", "/home/ezyang/.pearrc");
require_once 'Chiara/PEAR/Server.php';
require_once 'Chiara/PEAR/Server/Backend/DBDataObject.php';
require_once 'Chiara/PEAR/Server/Frontend/HTMLQuickForm.php';
$backend = new Chiara_PEAR_Server_Backend_DBDataObject('htmlpurifier.org',
    '/home/ezyang/htmlpurifier.org/Chiara_PEAR_Server_REST', array('database' => $DB_CREDENTIALS));
$frontend = new Chiara_PEAR_Server_Frontend_HTMLQuickForm('htmlpurifier.org', new HTML_QuickForm('channel'),
        'pear-server.php', '/home/ezyang/usr/temp');
$frontend->sessionStart();
$server = new Chiara_PEAR_Server('/home/ezyang/htmlpurifier.org/get');
$server->setBackend($backend);
$server->setFrontend($frontend);
$server->run();
?>
