#!/bin/bash
if [ ! -d phorum-original ]
then
    echo "No phorum-original directory found; please tar -xzf phorum-latest.tar.gz"
    echo "and rename result to phorum-original"
    exit
fi
cp -r phorum-original/* phorum
cd phorum
patch -u -p 1 < ../phorum.patch

