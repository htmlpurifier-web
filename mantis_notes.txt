
A Mantis/HTMLPurifier Installation

In my quest for a bugtracker, I think I've also found the ideal candidate
for the first integration with HTML Purifier.  Mantis uses HTML comments,
so it was quite easy to plug in TinyMCE + HTML Purifier to demonstrate just
what HTML Purifier can do.

All files and directories prefixed with an underscore are items added by
my customization. I also had to patch some core files:

core/string_api.php
    - string_display() : use only HTML Purifier
    - string_api() : rm string_insert_hrefs(), use HTMLPurifier
    - string_textarea() : use HTML Purifier before escaping
    - string_get_bugnote_view_link() : remove prepended 'Note: '

core/bugnote_api.php
    - bugnote_format_id() : prepend tag to format

core/bug_api.php
    - bug_format_id() : prepend tag to format

core/html_api.php
    - html_header() : do not output header HTML if no header is set

core/print_api.php
    - print_project_option_list() : do not use string_display() on project names

core.php
    - patched to make pages cacheable correctly with proper headers

changelog_page.php
    - rewritten to be less teletype oriented, since we are allowing html

css/default.css
    - removed invalid color declarations

lang/string_english.txt
    - changed encoding to UTF-8
    - renamed Change log to Versions page
    - added link to versions page explaining versions

Be sure to redo these customizations during upgrade. I owe it a lot to the
Mantis team for centralizing everything. This is procedural programming at
its finest.

Note that as a security measure, UTF-8 English is the only language allowed.
To expand language support, all the string files must be converted to UTF-8.