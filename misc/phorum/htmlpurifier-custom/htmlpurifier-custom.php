<?php

function phorum_htmlpurifier_custom_common() {
    global $PHORUM;
    $base = dirname(__FILE__) . '/../../..';
    $PHORUM['DATA']['HP_NAVIGATION'] = @file_get_contents($base . '/navigation.frag');
    
    function phorum_csrf_callback() {
        echo 'CSRF check failed. Please ensure cookies are enabled and try resubmitting.<br><br>';
        if (isset($_POST['body'])) {
            echo 'You posted:<br><pre>' . htmlspecialchars($_POST['body']) . '</pre>';
        }
    }
    
    function csrf_startup() {
        csrf_conf('rewrite-js', '/csrf-magic/csrf-magic.js');
        csrf_conf('callback', 'phorum_csrf_callback');
    }
    require_once $base . '/csrf-magic/csrf-magic.php';
}

