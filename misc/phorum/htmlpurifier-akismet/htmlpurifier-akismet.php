<?php

define("PHORUM_SPAM_DELETE_MESSAGE", 'spamdelmess');
define("PHORUM_SPAM_DELETE_TREE", 'spamdeltree');

/**
 * Performs spam check, and forcibly terminates execution of Phorum if failure.
 */
function phorum_akismet_check_spam($message){

    global $PHORUM;
    
    if(!$PHORUM["user"]["admin"] &&
        !$message["moderator_post"] &&
            ($PHORUM["user_id"] == 0 ||
                ($PHORUM["akismet"]["check_reg"] &&
                    ($PHORUM["akismet"]["min_posts"]==0 ||
                        $PHORUM["akismet"]["min_posts"] > $PHORUM["user"]["post_count"])))){

        $return = phorum_akismet_http_request( $message, "comment-check" );

        if($return=="true"){
            echo 'Akismet thinks your message is spam. You posted:<br /><br /><pre>';
            echo htmlspecialchars($message['body']);
            echo '</pre>';
            exit;
        }
    }

    return $message;
}

/**
 * Moderation hook for giving feedback to Akismet service
 */
function phorum_akismet_moderation($mod_step){
    switch($mod_step){
        case PHORUM_SPAM_DELETE_MESSAGE:
            // replace with next mod_step to be executed
            $mod_step = PHORUM_DELETE_MESSAGE;
            $message  = phorum_db_get_message($GLOBALS["msgthd_id"]);
            $return   = phorum_akismet_http_request( $message, "submit-spam" );
            break;
        case PHORUM_SPAM_DELETE_TREE:
            // replace with next mod_step to be executed
            $mod_step = PHORUM_DELETE_TREE;
            $message  = phorum_db_get_message($GLOBALS["msgthd_id"]);
            $return   = phorum_akismet_http_request( $message, "submit-spam" );
            break;
        case PHORUM_APPROVE_MESSAGE:
        case PHORUM_APPROVE_MESSAGE_TREE:
            $message = phorum_db_get_message($GLOBALS["msgthd_id"]);
            $return  = phorum_akismet_http_request( $message, "submit-ham" );
            break;
    }
    return $mod_step;
}

/**
 * Read hook for setting up URLs to submit appropriate spam for messages.
 */
function phorum_akismet_read_url_change($messages) {
    global $PHORUM;
    if($PHORUM["DATA"]["MODERATOR"]){
        foreach($messages as $key=>$message){
            $message["spam_url1"] = phorum_get_url(PHORUM_MODERATION_URL, PHORUM_SPAM_DELETE_MESSAGE, $message["message_id"]);
            $message["spam_url2"] = phorum_get_url(PHORUM_MODERATION_URL, PHORUM_SPAM_DELETE_TREE, $message["message_id"]);
            $messages[$key] = $message;
        }
    }
    return $messages;
}

function phorum_akismet_verify_key($key){
    return phorum_akismet_http_request( $key, "verify-key" );
}

/**
 * Generic HTTP request for Akismet server.
 */
function phorum_akismet_http_request( $message, $mode ) {

    global $PHORUM;
    $buf = '';

    if($mode == "verify-key"){
        $host = "rest.akismet.com";

        $data = "key=".urlencode($message)."&blog=".urlencode($PHORUM["http_path"]);

    } else {

        $host = $PHORUM["akismet"]["key"].".rest.akismet.com";

        $data = "blog=".urlencode($PHORUM["http_path"]);
        $data.= "&user_ip=".urlencode($_SERVER["REMOTE_ADDR"]);
        $data.= "&user_agent=".urlencode($_SERVER["HTTP_USER_AGENT"]);
        if (isset($_SERVER["HTTP_REFERER"])) {
            $data.= "&referrer=".urlencode($_SERVER["HTTP_REFERER"]);
        } else {
            // optional, but we'll include it anyway to signal it was empty
            $data.= "&referrer=";
        }
        if($message["thread"]){
            $data.= "&permalink=".urlencode(phorum_get_url(PHORUM_FOREIGN_READ_URL, $message["forum_id"], $message["thread"]));
        } else {
            $data.= "&permalink=".urlencode(phorum_get_url(PHORUM_LIST_URL, $message["forum_id"]));
        }
        $data.= "&comment_type=".urlencode("forum");
        $data.= "&comment_author=".urlencode($message["author"]);
        $data.= "&comment_author_email=".urlencode($message["email"]);
        $data.= "&comment_content=".urlencode($message["body"]);
    }

    $fp = @fsockopen( $host, 80, $errno, $errstr, 8 ); // timeout set to 8 seconds

    if (FALSE !== $fp) {
        stream_set_timeout ( $fp, 10 ); // timeout changed to 10 seconds (why?)

        // Although the Akismet server may be tolerant, the request as constructed
        // here did not conform to either RFC 1945 (HTTP 1.0) or RFC 2616 (HTTP 2616).
        // I've adapted the code to conform to (both) HTTP standards. MK 2007-03-17
        //
        // 1) RFC 1945: "HTTP/1.0 defines the octet sequence CR LF as the end-of-line
        //           marker for all protocol elements except the Entity-Body" - and:
        //           HTTP-header    = field-name ":" [ field-value ] CRLF
        //    RFC 2616: "HTTP/1.1 defines the sequence CR LF as the end-of-line marker
        //           for all protocol elements except the entity-body" - and:
        //           "generic-message = start-line
        //                 *(message-header CRLF)
        //                 CRLF
        //                 [ message-body ]
        //           start-line      = Request-Line | Status-Line"
        // MK: every \n in this section changed to \r\n
        //           cf. http://www.isi.edu/in-notes/rfc1945.txt
        //           cf. ftp://ftp.isi.edu/in-notes/rfc2616.txt
        //           cf. http://akismet.com/development/api/
        //
        // 2) RFC 1945: (HTTP/1.0) "When no explicit charset parameter is provided by
        //           the sender, media subtypes of the "text" type are defined to have
        //           a default charset value of "ISO-8859-1" when received via HTTP."
        //    RFC 2616: "HTTP/1.1 recipients MUST respect the charset label provided
        //           by the sender (...) When no explicit charset parameter is
        //           provided by the sender, media subtypes of the "text" type are
        //           defined to have a default charset value of "ISO-8859-1" when
        //           received via HTTP. Data in character sets other than "ISO-8859-1"
        //           or its subsets MUST be labeled with an appropriate charset value."
        // MK: this means that if Phorum is configured to use another charset than
        //           ISO-8859-1, the charset MUST be specified in the Content-type
        //           header! (And it's not wrong to specify ISO-8859-1.)
        //           cf. http://www.isi.edu/in-notes/rfc1945.txt
        //           cf. ftp://ftp.isi.edu/in-notes/rfc2616.txt
        //           cf. http://akismet.com/development/api/
        fputs( $fp, "POST /1.1/$mode HTTP/1.0\r\n" );
        fputs( $fp, "Host: $host\r\n" );
        fputs( $fp, "Content-type: application/x-www-form-urlencoded; charset=" . $PHORUM['DATA']['CHARSET'] . "\r\n" ); // charset added
        fputs( $fp, "Content-length: " . strlen( $data ) . "\r\n" );
        fputs( $fp, "User-Agent: Phorum ".PHORUM."\r\n" );
        fputs( $fp, "Connection: close\r\n\r\n" );
        fputs( $fp, $data );

        $x=1;
        while ( !feof( $fp ) ) {
            $buf .= fgets( $fp, 1024 );

            // if the fgets returns nothing on the first return,
            // the remote server is timing out.
            if($x==1 && $buf==""){
                $errstr="timeout waiting for data";
                break;
            }
            $x++;
        }

        fclose( $fp );

    }

    if (FALSE === $fp || empty($buf)){
        trigger_error("Could not open $host: $errstr", E_USER_WARNING);
    }

    $buf = str_replace( "\r", "", $buf );
    list( $page_data["headers"], $page_data["content"] ) = explode( "\n\n", $buf, 2 );

    return $page_data["content"];
}
