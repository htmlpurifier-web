Akismet Module
===============

To block spam
--------------
1. Go to Akismet.com and get a key.
2. Unpack the akismet module in your mods dir.
3. Go to your Phorum admin and turn the module on.
4. Edit the Settins for the module and enter you key.


To report spam
---------------
This is a little trickier.  You have to edit your template to insert the Spam
links on the read page.

In read.tpl, you will find something like: 

<a class="PhorumNavLink" href="javascript:if(window.confirm('{LANG->ConfirmDeleteThread}')) window.location='{MESSAGES->delete_url2}';">{LANG->DeleteThread}</a>

<a class="PhorumNavLink" href="javascript:if(window.confirm('{LANG->ConfirmDeleteMessage}')) window.location='{MESSAGES->delete_url1}';">{LANG->DeleteMessage}</a>

<a class="PhorumNavLink" href="javascript:if(window.confirm('{LANG->ConfirmDeleteMessage}')) window.location='{MESSAGES->delete_url2}';">{LANG->DelMessReplies}</a>

You will want to add another link just before each of those:

<a class="PhorumNavLink" href="javascript:if(window.confirm('{LANG->ConfirmDeleteThread}')) window.location='{MESSAGES->spam_url2}';">{LANG->Spam}</a>

<a class="PhorumNavLink" href="javascript:if(window.confirm('{LANG->ConfirmDeleteMessage}')) window.location='{MESSAGES->spam_url1}';">{LANG->Spam}</a>

<a class="PhorumNavLink" href="javascript:if(window.confirm('{LANG->ConfirmDeleteMessage}')) window.location='{MESSAGES->spam_url2}';">{LANG->Spam}</a>

These links will delete the spam and submit it to Akismet.

For read_threads.tpl, look for the same URLs.

