<?php

if(!defined("PHORUM_ADMIN")) return;

if(count($_POST)){

    if(empty($_POST["key"])){

        $error = "You must enter an Akismet key";

    } else {

        include_once dirname(__FILE__) . "/htmlpurifier-akismet.php";
        
        $return = phorum_akismet_verify_key($_POST["key"]);

        if($return != "valid"){

            $error = "The key you entered is not a valid Akismet key";

        } else {

            $akismet = array(
                "key" => $_POST["key"],
                "check_reg" => $_POST["check_reg"],
                "min_posts" => $_POST["min_posts"]
            );
            
            if(!phorum_db_update_settings(array("akismet"=>$akismet))){
                $error="Database error while updating settings.";
            } else {
                $PHORUM["akismet"] = $akismet;
                phorum_admin_okmsg("Akismet Settings Saved");
            }
            
        }
            
    }
    
}

if ( $error ) {
    phorum_admin_error( $error );
}

include_once "./include/admin/PhorumInputForm.php";

$frm =& new PhorumInputForm ("", "post", "Save");

$frm->hidden("module", "modsettings");
$frm->hidden("mod", "htmlpurifier-akismet");
$frm->addrow("Akismet Key<br /><small>See: <a href=\"http://akismet.com/\">Akismet.com</a> to get a key</small>", $frm->text_box("key", $PHORUM["akismet"]["key"], 50));
$frm->addrow( "Check Registered Users", $frm->select_tag( "check_reg", array( "No", "Yes" ), $PHORUM["akismet"]["check_reg"] ) );
$frm->addrow( "Skip user with this many posts or more:", $frm->text_box("min_posts", $PHORUM["akismet"]["min_posts"], 20) );

$frm->show();
