<?php
$PHORUM['DATA']['LANG']['ConfirmDeleteSpamMessage']   = 'Are you sure you want to delete this message and report it as spam?';
$PHORUM['DATA']['LANG']['ConfirmDeleteSpamSubthread'] = 'Are you sure you want to delete this message and all reactions and report them as spam?';
$PHORUM['DATA']['LANG']['ConfirmDeleteSpamThread']    = 'Are you sure you want to delete this thread and report it as spam?';
$PHORUM['DATA']['LANG']['SpamMessage']		= 'Spam Message';				# message
$PHORUM['DATA']['LANG']['SpamMessageShort']	= 'Spam';						# message
$PHORUM['DATA']['LANG']['SpamSubThread']	= 'Subthread is Spam';			# subthread
$PHORUM['DATA']['LANG']['SpamThread']		= 'Spam Thread';				# thread
?>