<?php
$PHORUM['DATA']['LANG']['ConfirmDeleteSpamMessage']   = 'Weet je zeker dat je dit bericht wilt verwijderen en rapporteren als spam?';
$PHORUM['DATA']['LANG']['ConfirmDeleteSpamSubthread'] = 'Weet je zeker dat je dit bericht met alle reacties wilt verwijderen en reapporteren als spam?';
$PHORUM['DATA']['LANG']['ConfirmDeleteSpamThread']    = 'Weet je zeker dat je deze discussie wilt verwijderen en repporteren als spam?';
$PHORUM['DATA']['LANG']['SpamMessage']		= 'Bericht is Spam';			# message
$PHORUM['DATA']['LANG']['SpamMessageShort']	= 'Spam';						# message
$PHORUM['DATA']['LANG']['SpamSubThread']	= 'Spam inclusief reacties';	# subthread
$PHORUM['DATA']['LANG']['SpamThread']		= 'Discussie is spam';			# thread
?>